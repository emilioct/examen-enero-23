from check_sensor import check_sensor
def test_A(mocker):
	sensor = mocker.Mock()
	sensor.read.return_value = [2,2]
	assert check_sensor (sensor) == 2
def test_B(mocker):
	sensor = mocker.Mock()
	sensor.read.return_value = [2,2,3,4,5,2,2]
	assert check_sensor (sensor) == 4
def test_C(mocker):
	sensor = mocker.Mock()
	sensor.read.return_value = []
	assert check_sensor (sensor) == 0
def test_D(mocker):
	sensor = mocker.Mock()
	sensor.read.return_value = [1,2,3]
	assert check_sensor (sensor) == 1	

