def check_sensor(s):
	sequence = s.read()
	longest_seq_length = 0
	for i in range(len(sequence)):
		current_number = sequence[i]
		current_seq_length = 1
		for j in range(i+1, len(sequence)):
			if sequence[j] == current_number:
				current_seq_length += 1
			else:
				break
		if current_seq_length < longest_seq_length:
			continue
		for j in range(i):
			if sequence[j] == current_number:
				current_seq_length += 1
			else:
				break
		if current_seq_length > longest_seq_length:
			longest_seq_length = current_seq_length
	return longest_seq_length
